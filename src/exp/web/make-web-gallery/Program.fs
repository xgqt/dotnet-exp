(*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 *)

(*
 * Create a index.html gallery given a directory path.
 *)


open System.IO
open System.Text.RegularExpressions


let MakeHtml (title : string) (insides : string) =
    $"""<!DOCTYPE html>
<html>
  <head>
    <title>{title}</title>
    <meta charset="utf-8"
          content="text/html"
          http-equiv="content-type">
  <head>
  <body>
    <center>
      {insides}
    </center>
  </body>
</html>"""
    ;;

let MakeImageTd (image_path : string) =
    $"""<td width="22%%" align="center" valign="baseline">
  <a href="{image_path}">
    <img src="{image_path}"
         alt="{image_path}"
         hspace="7" vspace="7"
         width="120" height="90">
    <br />
  </a>
</td>"""
    ;;

let MakeImageTable (image_list : string [] list) =
    image_list
    |> Seq.map (fun l -> String.concat "\n" l)
    |> Seq.map (fun i -> $"<tr>\n{i}\n</tr>")
    |> String.concat "\n"
    |> fun s -> "<table>\n" + s + "\n</table>"
    ;;


[<EntryPoint>]
let main argv =
    for arg in argv do
        printfn $">>> {arg}"
        Directory.GetFiles arg
        |> Seq.filter (fun s -> Regex.IsMatch(s, ".*.(jpg|png|svg)"))
        |> Seq.map MakeImageTd
        |> Seq.chunkBySize 4  // TODO: Columns parameter
        |> Seq.toList
        |> MakeImageTable
        |> MakeHtml arg
        |> fun s -> File.WriteAllText(arg + "/index.html", s)
    0
    ;;
