using System;

namespace lab9
{
    public class Sigmoid : IFunctional
    {
        public double calc(double x)
        {
            return 1 / (1 + Math.Exp(-x));
        }

        public double backward(double y)
        {
            // FIXME:
            return y;
        }
    }
}
