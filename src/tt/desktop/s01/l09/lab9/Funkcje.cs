using System;

namespace lab9
{
    public class Funkcje
    {
        public static double ReLu(double x)
        {
            return x < 0 ? 0 : x;
        }

        public static double Sigmoid(double x)
        {
            return 1 / (1 + Math.Exp(-x));
        }
    }
}
