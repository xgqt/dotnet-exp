using System;
using System.Collections.Generic;
using System.Text;

namespace lab9
{
    public class Relu : IFunctional
    {
        public double calc(double x)
        {
            return x < 0 ? 0 : x;
        }

        public double backward(double y)
        {
            return y < 0 ? 0 : 1;
        }
    }
}
