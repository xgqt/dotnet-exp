using System;

namespace lab9
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Neuron n = new Neuron {
                    f = new Relu().calc,
                    weights = new double[3] { 1, 2, 3 }
                };
            }
            catch (Exception exn)
            {
                Console.WriteLine(exn);
            }
        }
    }
}
