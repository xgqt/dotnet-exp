namespace lab9
{
    public class MyMath
    {
        /// <summary>
        /// Funkcja Suma
        /// </summary>
        /// <param name="a">liczba</param>
        /// <param name="b">liczba</param>
        /// <returns>wynik</returns>
        public static double Suma(double a, double b)
        {
            return a + b;
        }
    }
}
