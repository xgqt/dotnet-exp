using System;
using System.Linq;

namespace lab9
{
    public class Neuron
    {
        /// tablice 1-wymiarowe
        public double[] weights { get; set; }
        /// obiekt delegata do funkcji nieliniowej f
        public Func<double, double> f { get; set; }
        public IFunctional fun { get; set; }

        /// przesunięcie
        private double bias;
        /// pośredni wynik obliczeń (suma ważona)
        private double s;

        /// <summary>
        /// funkcja zwracająca wyjście y
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public double calc(double[] input)
        {
            int n = weights.Length;
            s = 0;

            for (int i = 0; i < n;  i ++)
            {
                s += weights[i] * input[i];
            }

            s += bias;

            return f(s);
        }

        /// <summary>
        /// Zwraca pochodne dy/dx potrzebne do algorytmu uczenia
        /// Wynik jest tablicą [d0, d1, ...]
        /// Dla każdej wagi d_i
        /// d_i = weights[i] * fun.backward(y)
        /// </summary>
        /// <param name="y"></param>
        /// <returns>wyjście y</returns>
        public double[] backward(double y)
        {
            return this.weights
                .Select(weight => weight * fun.backward(y)).ToArray();
        }
    }
}
