using System;
using System.Collections.Generic;
using System.Text;

namespace lab9
{
    public interface IFunctional
    {
        /// <summary>
        /// obliczenie funkcji
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public double calc(double x);

        /// <summary>
        /// obliczenie pochodnej dy/dx
        /// </summary>
        /// <param name="y"></param>
        /// <returns></returns>
        public double backward(double y);
    }
}
