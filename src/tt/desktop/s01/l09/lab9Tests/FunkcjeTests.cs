using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace lab9.Tests
{
    [TestClass()]
    public class FunkcjeTests
    {
        [TestMethod()]
        public void ReLuForZero()
        {
            Assert.AreEqual(0, Funkcje.ReLu(0));
        }

        [TestMethod()]
        public void ReLuForLessThanZero()
        {
            Assert.AreEqual(0, Funkcje.ReLu(-1));
        }

        [TestMethod()]
        public void ReLuForMoreThanZero()
        {
            Assert.AreEqual(1, Funkcje.ReLu(1));
        }

        [TestMethod()]
        public void SigmoidForZero()
        {
            Assert.AreEqual(0.5, Funkcje.Sigmoid(0));
        }

        [TestMethod()]
        public void SigmoidForLessThanZero()
        {
            Assert.AreEqual(0.2, Funkcje.Sigmoid(-1), 0.09);
        }

        [TestMethod()]
        public void SigmoidForMoreThanZero()
        {
            Assert.AreEqual(0.7, Funkcje.Sigmoid(1), 0.09);
        }
    }
}
