using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace lab9.Tests
{
    [TestClass()]
    public class MyMathTests
    {
        [TestMethod()]
        public void SumaTest1()
        {
            double a = 1.0;
            double b = 3;
            double result = MyMath.Suma(a, b);
            Assert.AreEqual(result,4, 1e-6);
        }

        [TestMethod()]
        public void SumaTest2()
        {
            double a = 10;
            double b = 0.01;
            double result = MyMath.Suma(a, b);
            Assert.AreEqual(result, 10.01, 1e-6);
        }

        [TestMethod()]
        public void SumTest3()
        {
            Assert.AreEqual(3, MyMath.Suma(1, 2), 1e-6);
        }

        [TestMethod()]
        public void SumTest4()
        {
            Assert.AreEqual(0, MyMath.Suma(0, 0), 1e-6);
        }
    }
}
