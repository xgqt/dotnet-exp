/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace binding
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
    }
}
