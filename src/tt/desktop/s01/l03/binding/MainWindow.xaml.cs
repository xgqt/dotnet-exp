/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace binding
{
    public partial class MainWindow : Window
    {
        private ObservableCollection<Point> points;

        public MainWindow()
        {
            InitializeComponent();

            points = new ObservableCollection<Point>()
            {
                new Point() { X="1", Y="2" },
                new Point() { X="3", Y="4" },
                new Point() { X="5", Y="6" }
            };
            this.DataContext = points[0];

            PointsList.ItemsSource = points;
            PointsList2.ItemsSource = points;
            PointsList3.ItemsSource = points;
        }

        private void PointsListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PointsList.SelectedIndex >= 0)
                this.DataContext = points[PointsList.SelectedIndex];
        }
    }

    class Point : INotifyPropertyChanged
    {
        // public string X { get; set; }
        // public string Y { get; set; }

        private int x;
        private int y;

        public string Data
        {
            get { return ToString(); }
        }

        public override string ToString()
            => string.Format($"{X},{Y}");

        public event PropertyChangedEventHandler PropertyChanged;

        public string X
        {
            get { return x.ToString(); }
            set
            {
                try
                {
                    if (x.ToString() != value)
                    {
                        x = int.Parse(value);
                        this.NotifyPropertyChanged("X");
                        this.NotifyPropertyChanged("Data");
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    Console.WriteLine(e.StackTrace);
                }
            }
        }

        public string Y
        {
            get { return y.ToString(); }
            set
            {
                try
                {
                    if (y.ToString() != value)
                    {
                        y = int.Parse(value);
                        this.NotifyPropertyChanged("Y");
                        this.NotifyPropertyChanged("Data");
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    Console.WriteLine(e.StackTrace);
                }
            }
        }

        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }
}
