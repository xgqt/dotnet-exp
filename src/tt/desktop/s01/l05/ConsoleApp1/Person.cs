/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


namespace ConsoleApp1
{
    class Person
    {
        public int Year { set; get; }
        public string Name { set; get; }
        public string Surname { set; get; }
    }
}
