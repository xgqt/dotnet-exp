/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


﻿using System;
using System.Linq;
using System.Collections.Generic;


namespace ConsoleApp1
{
    class Program
    {
        // 1.
        delegate int MyDelegate(int a, int b, int c);


        static void Main(string[] args)
        {
            // 1. Napisz dowolny delegat i do obiektu delegata przypisz
            //    wyrażenie lambda, a później je wywołaj.
            Console.WriteLine("// 1.");

            MyDelegate mydelegate = ((a, b, c) => ((a + b) * c));

            Console.WriteLine(mydelegate(1, 2, 3));


            // 2 - 4
            List<int> lista002 = new List<int>();
            lista002.AddRange(Enumerable.Range(1, 20));


            // 2. Kolekcja zawierająca liczby
            //    wypisz elementy parzyste

            Console.WriteLine("// 2.");

            foreach (int i in (from i in lista002 where i % 2 == 0 select i))
            {
                Console.WriteLine(i);
            }


            // 3. Kolekcja zawierająca liczby
            //    wypisz najmniejszą wartość spośród elementów parzystych

            Console.WriteLine("// 3.");

            Console.WriteLine(lista002.Where(n => n % 2 == 0).Min());


            // 4. Kolekcja zawierająca liczby
            //    wypisz elementy z podanego zakresu

            Console.WriteLine("// 4.");

            foreach (int i in (lista002.Where(n => n > 5 && n < 15)))
            {
                Console.WriteLine(i);
            }


            // 5 - 7
            List<Person> list2 = new List<Person>()
            {
                new Person(){ Year=1900, Name="Jan",     Surname="Kowalski"},
                new Person(){ Year=1950, Name="Janina",  Surname="Nowak"},
                new Person(){ Year=1999, Name="Jaromir", Surname="Nowakowski"}
            };


            // 5. Kolekcja osoby
            //    wypisz osoby urodzone po podanym roku

            Console.WriteLine("// 5.");

            foreach (Person p in (list2.Where(p => p.Year > 1920)))
            {
                Console.WriteLine($"{p.Name} {p.Surname} ({p.Year})");
            }


            // 6. Kolekcja osoby
            //    wypisz osoby, których imię zaczyna się od podanego ciągu
            //    (np.2 litery),
            //    posortuj w kolejności alfabetycznej po nazwisku

            Console.WriteLine("// 6.");

            foreach (Person p in (list2
                                 .Where(p => p.Name.StartsWith("Jan"))
                                 .OrderBy(p => p.Surname)))
            {
                Console.WriteLine($"{p.Name} {p.Surname} ({p.Year})");
            }


            // 7. Jak -wyżej - ile jest takich osób

            Console.WriteLine("// 7.");

            Console.WriteLine((list2
                               .Where(p => p.Name.StartsWith("Jan")).Count()));


            // 8. Napisz funkcję - algorytm,
            //    który jako dane wejściowe przyjmuje dwie kolekcje,
            //    zwraca kolekcję wynikową.
            //    Dla elementu o indeksie i kolekcji 1
            //    i kolekcji 2 generuje element o indeksie
            //    i kolekcji wynikowej.
            //    Sposób wyliczenia wyniku określony jest przez delegat.

            Console.WriteLine("// 8.");

            List<int> list3a = new List<int> { 9, 8, 7 };
            List<int> list3b = new List<int> { 6, 5, 4 };

            foreach (int n in MyJoin(((a, b) => a + b), list3a, list3b))
            {
                Console.WriteLine($"a + b = {n}");
            }

            foreach (int n in MyJoin(((a, b) => a - b), list3a, list3b))
            {
                Console.WriteLine($"a - b = {n}");
            }

            foreach (int n in MyJoin(((a, b) => a * b), list3a, list3b))
            {
                Console.WriteLine($"a * b = {n}");
            }
        }


        // Zrobione metodą "młotka", pewnie można też użyć Zip, Map, ...

        static List<int> MyJoin(Func<int, int, int> func,
                                List<int> listA, List<int> listB)
        {
            int lenA = listA.Count();
            int lenB = listB.Count();

            if (lenA != lenB)
            {
                throw new Exception($"Wrong list lengths: {lenA} and {lenB}");
            }

            List<int> ret = new List<int>();

            for (int i = 0; i < lenA; i++)
            {
                ret.Add(func(listA[i], listB[i]));
            }

            return ret;
        }
    }
}
