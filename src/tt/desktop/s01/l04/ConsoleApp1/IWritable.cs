/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System;


namespace ConsoleApp1
{
    interface IWritable
    {
        void wypisz();
    }
}
