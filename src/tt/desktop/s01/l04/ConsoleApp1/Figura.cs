/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System;


namespace ConsoleApp1
{
    public abstract class Figura
    {
        public abstract void info();
        public abstract double pole();
    }
}
