/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System;


namespace ConsoleApp1
{
    class Prostokat : Figura
    {
        protected double a;
        protected double b;

        public double A
        {
            get { return a; }
            set { a = value; }
        }
        public double B
        {
            get { return b; }
            set { b = value; }
        }

        public override void info()
        {
            Console.WriteLine($"A: {a} | B: {b}");
        }

        public override double pole()
        {
            return (a * b);
        }

        public Prostokat(double a, double b)
        {
            this.a = a;
            this.b = b;
        }

        public Prostokat()
        {
            this.a = 0;
            this.b = 0;
        }
    }
}
