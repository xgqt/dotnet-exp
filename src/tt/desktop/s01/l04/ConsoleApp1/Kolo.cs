/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System;


namespace ConsoleApp1
{
    class Kolo : Figura
    {
        protected double r;

        public double R
        {
            get { return r; }
            set { r = value; }
        }

        public override void info()
        {
            Console.WriteLine($"R: {r}");
        }

        public override double pole()
        {
            return (Math.PI * r * r);
        }

        public Kolo(double r)
        {
            this.r = r;
        }

        public Kolo()
        {
            this.r = 0;
        }
    }
}
