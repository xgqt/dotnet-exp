/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System;


namespace ConsoleApp1
{
    class Pojazd : IWritable
    {
        protected string nr_rej;
        protected string marka;
        protected int liczba_kol;

        public string NrRej
        {
            get { return nr_rej; }
            set { nr_rej = value; }
        }

        public string Marka
        {
            get { return marka; }
            set { marka = value; }
        }

        public int LiczbaKol
        {
            get { return liczba_kol; }
            set { liczba_kol = value; }
        }

        public Pojazd(string nr_rej, string marka, int liczba_kol)
        {
            this.nr_rej = nr_rej;
            this.marka = marka;
            this.liczba_kol = liczba_kol;
        }

        public virtual void wypisz()
        {
            Console.WriteLine("{0} {1} {2}", nr_rej, marka, liczba_kol);
        }

        public void SetNrRej(string nr_rej)
        {
            this.nr_rej = nr_rej;
        }

        public string GetNrRej()
        {
            return nr_rej;
        }
    }
}
