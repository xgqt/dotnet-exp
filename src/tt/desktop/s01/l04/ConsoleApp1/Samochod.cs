/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System;
using System.Collections.Generic;


namespace ConsoleApp1
{
    class Samochod : Pojazd
    {
        private int liczba_miejsc;

        public int LiczbaMiejsc
        {
            get { return liczba_miejsc; }
            set { liczba_miejsc = value; }
        }

        /// <summary>
        /// Konstruktor klasy Samochod
        /// </summary>
        /// <param name="nr_rej">Numer rejestracyjny</param>
        /// <param name="marka">Marka </param>
        /// <param name="liczba_kol">Liczba kół samochodu</param>
        /// <param name="liczba_miejsc">Liczba miejsc samochodu</param>
        public Samochod(
            string nr_rej, string marka, int liczba_kol, int liczba_miejsc)
            : base(nr_rej, marka, liczba_kol)
        {
            this.liczba_miejsc = liczba_miejsc;
        }

        public override void wypisz()
        {
            Console.WriteLine(
                $"{nr_rej} {marka} {liczba_kol} {liczba_miejsc}");
        }

        public override string ToString()
        {
            return $"{nr_rej} {marka} {liczba_kol} {liczba_miejsc}";
        }
    }
}
