/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System;
using System.Collections.Generic;


namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Pojazd
            {
                Pojazd p = new Pojazd("RZ12345", "Fiat", 4);

                p.wypisz();
                p.SetNrRej("KR10000");

                Console.WriteLine("Nr rej: " + p.GetNrRej());
                Console.Write("Naciśnij dowolny klawiasz...");
                Console.ReadKey();
            }

            // Punkt
            {
                Punkt a = new Punkt();
                Punkt b = new Punkt() { X = 1, Y = 2 };
            }

            // Samochod
            {
                Samochod s = new Samochod("RZ10101", "Hundai", 4, 5);

                s.wypisz();

                Console.Write("Po zmianie NrRej: ");
                s.NrRej = "RZ10102";
                s.wypisz();
            }

            // Pojazd + Samochod
            {
                Pojazd p1 = new Samochod("RZ20000", "BMW", 4, 3);
                int m = (p1 as Samochod).LiczbaMiejsc;
                Console.WriteLine("p1 jest klasy Samochod, p1.LiczbaMiejsc:"
                                  + (p1 as Samochod).LiczbaMiejsc);

                Pojazd p2 = p1;
                p2.wypisz();

                // Interfejsy
                IWritable wr = p1;
                Console.Write("Metoda Interfejsu: ");
                wr.wypisz();
            }

            // Tablice
            {
                int[] tablica = new int[10];

                Console.WriteLine($"tablica[0] {tablica[0]}");
                Console.WriteLine("tablica[1]:" + tablica[1]);

                int[] tablica2 = new int[] { 5, 10, 15, 100 };

                // Foreach
                foreach (int el in tablica)
                {
                    Console.Write(el + ", ");
                }
                Console.WriteLine();

                // For
                for (int i = 0; i < tablica.Length; i++)
                {
                    Console.Write(tablica[i] + ", ");
                }
            }

            // Kolekcje
            {
                List<int> lista0 = new List<int>();
                List<int> lista = new List<int>() { 100, 3, 1000, -700 };

                lista.Add(5);

                Console.WriteLine($"lista[0] {lista[0]}");
                Console.WriteLine("lista[1]: " + lista[1]);
                Console.WriteLine("lista[2] {0}", lista[2]);

                for (int i = 0; i < lista.Count; i++)
                    Console.Write(lista[i] + ", ");
                Console.WriteLine();

                foreach (var el in lista)
                {
                    Console.Write(el + ", ");
                }

                int sum = 0;
                foreach (var el in lista)
                    sum += el;
                Console.WriteLine($"Suma: {sum}");

                // List + Punkt

                List<Punkt> lista1 = new List<Punkt>
                {
                    new Punkt{ X=1, Y=2 },
                    new Punkt{ X=3, Y=4 },
                    new Punkt{ X=5, Y=6 },
                };

                foreach (var p in lista1)
                    Console.WriteLine($"{p.X} {p.Y}");
            }

            // Prostokat
            {
                Prostokat p1 = new Prostokat();
                Console.Write("Prostokat p1 przed modyfikacją: ");
                p1.info();
                p1.A = 3;
                p1.B = 4;
                Console.Write("Prostokat p1 po modyfikacji: ");
                p1.info();

                Prostokat p2 = new Prostokat(5, 6);
                Console.Write("Prostokat p2: ");
                p2.info();
            }

            // Figura
            {
                Figura f1 = new Prostokat(1, 2);
                Console.Write("Figura f1: ");
                f1.info();
                Console.WriteLine($"Pole f1: {f1.pole()}");

                Figura f2 = new Kolo(3);
                Console.Write("Figura f2: ");
                f2.info();
                Console.WriteLine($"Pole f2: {f2.pole()}");
            }

            // Kolekcje: Prostokat i Kolo
            {
                List<Figura> lista = new List<Figura>()
                {
                    new Kolo(1),
                    new Kolo(2),
                    new Kolo(3),
                    new Prostokat(1, 1),
                    new Prostokat(1, 2),
                    new Prostokat(1, 3),
                };

                Console.WriteLine("Figury o polu mniejszym niz 4:");
                foreach (var item in lista)
                {
                    if (item.pole() < 4)
                    {
                        item.info();
                    }
                }
            }
        }
    }
}
