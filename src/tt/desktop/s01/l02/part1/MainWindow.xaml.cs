/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace part1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void RBBlueChecked(object sender, RoutedEventArgs e)
        {
            RBBlue.Foreground = Brushes.Blue;
        }

        private void RBGreenChecked(object sender, RoutedEventArgs e)
        {
            RBGreen.Foreground = Brushes.Green;
        }

        private void RBRedChecked(object sender, RoutedEventArgs e)
        {
            RBRed.Foreground = Brushes.Red;
        }

        private void BoldFontChecked(object sender, RoutedEventArgs e)
        {
            BoldFont.FontWeight = FontWeights.Bold;
        }

        private void BoldFontUnchecked(object sender, RoutedEventArgs e)
        {
            BoldFont.FontWeight = FontWeights.Normal;
        }

        private void ItalicFontChecked(object sender, RoutedEventArgs e)
        {
            ItalicFont.FontStyle = FontStyles.Italic;
        }

        private void ItalicFontUnchecked(object sender, RoutedEventArgs e)
        {
            ItalicFont.FontStyle = FontStyles.Normal;
        }

        private void MenuFileNewClick(object sender, RoutedEventArgs e)
        {
            TabItem TINew = new TabItem();
            TCMain.Items.Add(TINew);
            TINew.Content = new Grid();
            TINew.Header = "Dynamiczny Tab";
            
            SolidColorBrush brush = new SolidColorBrush()
            {
                Color = Color.FromRgb(0xE5, 0xE5, 0xE5)
            };

            (TINew.Content as Grid).Background = brush;
            
            Label LBLocal = new Label();
            LBLocal.Content = "Moja zwartość";
            LBLocal.Margin = new Thickness(20, 50, 0, 0);
            
            (TINew.Content as Grid).Children.Add(LBLocal);
        }
    }
}
