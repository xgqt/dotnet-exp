/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System;
using System.Windows.Media.Imaging;
using System.IO;

namespace part2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow() => InitializeComponent();

        private void ChangeLBOneColorToBlack(object sender, RoutedEventArgs e)
            => LBOne.Foreground = Brushes.Black;

        private void ChangeLBOneColorToGreen(object sender, RoutedEventArgs e)
            => LBOne.Foreground = Brushes.Green;

        private void ChangeLBOneColorToRed(object sender, RoutedEventArgs e)
            => LBOne.Foreground = Brushes.Red;

        private void ChangeLBOneFontWeightToBold(object sender, RoutedEventArgs e)
            => LBOne.FontWeight = FontWeights.Bold;

        private void RestoreLBOneFontWeightFromBold(object sender, RoutedEventArgs e)
            => LBOne.FontWeight = FontWeights.Normal;

        private void ChangeLBOneFontStyleToItalic(object sender, RoutedEventArgs e)
            => LBOne.FontStyle = FontStyles.Italic;

        private void RestoreLBOneFontStyleFromItalic(object sender, RoutedEventArgs e)
            => LBOne.FontStyle = FontStyles.Normal;

        private void ShowImage(object sender, RoutedEventArgs e)
        {
            // System.Drawing.Image & Image.FromFile are unavailable for me, no idea why
            ImageBrush pic = new ImageBrush();
            string pic_src = null;

            if (File.Exists("../../../pic.png"))
                pic_src = "../../../pic.png";
            else if (File.Exists("pic.png"))
                pic_src = "pic.png";

            if (pic_src is not null)
            {
                pic.ImageSource = new BitmapImage(new Uri(pic_src, UriKind.Relative));
                GImage.Background = pic;
            }
        }

        private void ShowCursorPosition(object sender, MouseEventArgs e)
            => LBShowCursorPosition.Content = e.GetPosition(relativeTo: LBShowCursorPosition).ToString();
    }
}
