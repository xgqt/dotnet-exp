/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System;
using System.Windows;

namespace sum
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            TBResult.IsReadOnly = true;
        }

        private void CalculateSum(object sender, RoutedEventArgs evt)
        {
            try
            {
                int x = Int32.Parse(TBX.Text);
                int y = Int32.Parse(TBY.Text);
                int z = Int32.Parse(TBZ.Text);
                TBResult.Text = (x + y + z).ToString();
            }
            catch (Exception ex)
            {
                TBResult.Text = "Błąd podczas liczenia sumy";
                MessageBox.Show(ex.Message);
            }
        }

        private void CalculateProduct(object sender, RoutedEventArgs evt)
        {
            try
            {
                int x = Int32.Parse(TBX.Text);
                int y = Int32.Parse(TBY.Text);
                int z = Int32.Parse(TBZ.Text);
                TBResult.Text = (x * y * z).ToString();
            }
            catch (Exception ex)
            {
                TBResult.Text = "Błąd podczas liczenia iloczynu";
                MessageBox.Show(ex.Message);
            }
        }
    }
}
