/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System;
using System.Windows;

namespace calc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private enum CalculationType
        {
            Summation,
            Substraction,
            Multiplication,
            Division,
            Identity
        }

        private int? maybeLastNumber = null;

        public MainWindow()
        {
            InitializeComponent();
            TBCalculation.IsReadOnly = true;
        }

        private void Calculate(CalculationType currentCalculation)
        {
            try
            {
                int currentInput = Int32.Parse(TBInput.Text);
                int lastInput;
                if (!maybeLastNumber.Equals(null))
                    lastInput = (int)maybeLastNumber;
                else
                    lastInput = 0;
                string calculation = "";
                int result = 0;

                switch (currentCalculation)
                {
                    case CalculationType.Summation:
                        calculation = $"{lastInput}+{currentInput}=";
                        result = lastInput + currentInput;
                        break;
                    case CalculationType.Substraction:
                        calculation = $"{lastInput}-{currentInput}=";
                        result = lastInput - currentInput;
                        break;
                    case CalculationType.Multiplication:
                        calculation = $"{lastInput}*{currentInput}=";
                        result = lastInput * currentInput;
                        break;
                    case CalculationType.Division:
                        calculation = $"{lastInput}/{currentInput}=";
                        result = lastInput / currentInput;
                        break;
                    case CalculationType.Identity:
                        calculation = $"{currentInput}=";
                        result = currentInput;
                        break;
                }

                TBCalculation.Text = calculation;
                TBInput.Text = result.ToString();
                maybeLastNumber = result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CalculateSummation(object sender, RoutedEventArgs evt)
        {
            Calculate(CalculationType.Summation);
        }

        private void CalculateSubstraction(object sender, RoutedEventArgs evt)
        {
            Calculate(CalculationType.Substraction);
        }

        private void CalculateMultiplication(object sender, RoutedEventArgs evt)
        {
            Calculate(CalculationType.Multiplication);
        }

        private void CalculateDivision(object sender, RoutedEventArgs evt)
        {
            Calculate(CalculationType.Division);
        }

        private void CalculateIdentity(object sender, RoutedEventArgs evt)
        {
            Calculate(CalculationType.Identity);
        }
    }
}
