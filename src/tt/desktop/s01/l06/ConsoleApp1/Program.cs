/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


﻿using System;
using System.Linq;
using ConsoleApp1.DAL;
using ConsoleApp1.Models;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SchoolContext db = new SchoolContext())
            {
                Console.WriteLine("Database initialization done.");

                System.Collections.Generic.List<Student> 
                    all_stdents = db.Students.ToList();
                System.Collections.Generic.List<Enrollment> 
                    all_enrollments = db.Enrollments.ToList();
                System.Collections.Generic.List<Course> 
                    all_courses = db.Courses.ToList();

                Console.WriteLine("");
                Console.WriteLine("01. Wszyscy studenci");
                foreach (Student i in all_stdents)
                {
                    Console.Write($"  {i.ID} | {i.LastName} | {i.FirstMidName} | {i.EnrollmentDate} |");
                    foreach (Enrollment en in i.Enrollments)
                    {
                        Console.Write($" {en.Course.Title}");
                    }
                    Console.WriteLine("");
                }

                Console.WriteLine("");
                Console.WriteLine("02. Wszyscy studenci na literę A");
                foreach (Student i in all_stdents.Where(s => (s.LastName != "" && s.LastName[0] == 'A')))
                {
                    Console.WriteLine($"  {i.ID} | {i.LastName} | {i.FirstMidName}");
                }

                Console.WriteLine("");
                Console.WriteLine("03. Studenci, ich oceny, id zapisu na przedmiot");
                foreach (var i in all_stdents
                                    .Join(all_enrollments,
                                          student => student.ID,
                                          enrollment => enrollment.StudentID,
                                          (student, enrollment) => new { Student = student, Enrollment = enrollment }))
                {
                    Console.WriteLine($"  {i.Student.ID} | {i.Student.LastName} | {i.Student.FirstMidName} | {i.Enrollment.Grade} | {i.Enrollment.CourseID}");
                }

                Console.WriteLine("");
                Console.WriteLine("04. Imiona i nazwiska studentów oraz nazwy przedmiotów na które są zapisani");
                foreach (var i in all_stdents
                                    .Join(all_enrollments,
                                          student => student.ID,
                                          enrollment => enrollment.StudentID,
                                          (student, enrollment) => new { Student = student, Enrollment = enrollment }))
                {
                    Console.WriteLine($"  {i.Student.ID} | {i.Student.LastName} | {i.Student.FirstMidName} | {i.Enrollment.Course.Title}");
                }

                Console.WriteLine("");
                Console.WriteLine("05. Imiona i nazwiska studentów nazwy przedmiotów i oceny które są wyższe niż F");
                foreach (var i in all_stdents
                                    .Join(all_enrollments,
                                          student => student.ID,
                                          enrollment => enrollment.StudentID,
                                          (student, enrollment) => (Student: student, Enrollment: enrollment))
                                    .Where(o =>
                                    {
                                        string s = o.Enrollment.Grade.ToString();
                                        return (s != "") && (s[0] < 'F');  // A < F
                                }))
                {
                    Course C = all_courses.Single(c => c.CourseID == i.Enrollment.CourseID);
                    Console.WriteLine($"  {i.Student.ID} | {i.Student.LastName} | {i.Student.FirstMidName} | {C.Title} | {i.Enrollment.Grade}");
                }
            }


            Console.WriteLine("");
            Console.WriteLine("Tasks done. Waiting for a key press...");
            Console.ReadKey();
        }
    }
}
