/*
 * (c) 2022 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of MIT license
 * SPDX-License-Identifier: MIT
 */


using System;
using System.Data.Entity;
using ConsoleApp1.Models;
using System.Linq;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ConsoleApp1.DAL
{
    public class SchoolContext : DbContext
    {
        // Your context has been configured to use a 'School' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ConsoleApp1.School' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'School' 
        // connection string in the application configuration file.
        public SchoolContext()
            : base("name=School")
        {
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Course> Courses { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        //public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int id { get; set; }
    //    public string name { get; set; }
    //}
}