#!/usr/bin/env python3


# (c) 2022 Maciej Barć <xgqt@riseup.net>
# Licensed under the terms of MIT license
# SPDX-License-Identifier: MIT


import argparse

from pathlib import Path
from typing import Optional


def generate_license_header(style: str) -> str:
    """Indent license info in a selected style way."""
    content = [
        "(c) 2022 Maciej Barć <xgqt@riseup.net>",
        "Licensed under the terms of MIT license",
        "SPDX-License-Identifier: MIT"
    ]
    return {
        "c":
        (lambda c: f"""/*
 * {c[0]}
 * {c[1]}
 * {c[2]}
 */
"""),
        "ml":
        (lambda c: f"""(*
 * {c[0]}
 * {c[1]}
 * {c[2]}
 *)
"""),
        "sh":
        (lambda c: f"""# {c[0]}
# {c[1]}
# {c[2]}
"""),
        "xml":
        (lambda c: f"""<!--
     {c[0]}
     {c[1]}
     {c[2]}
-->
""")
    }[style](content) + "\n"


def extension2style(extension: str) -> Optional[str]:
    """Given a extension pick a style.
    "cs" -> "c"
    "c"  -> "c"
    "_"  -> None
    """
    relation = {
        "c": ["cs"],
        "ml": ["fs"],
        "sh": ["py"],
        "xml": ["aspx", "csproj", "xaml"]
    }
    for key, value in relation.items():
        if extension in [key] + value:
            return key


def main() -> None:
    """Main."""

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--path", type=str,
                        default="src", help="path to search")
    parser.add_argument("extensions", type=str, nargs="*")
    args = parser.parse_args()

    if args.extensions == []:
        extensions = ["cs", "fs", "py", "sh", "xaml"]
    else:
        extensions = args.extensions

    # Small procedure to find files with given extension
    # extension -> ListOf found_paths extension
    find = (lambda ext: [Path(args.path).rglob(f"*.{ext}"), ext])

    for found, extension in list(map(find, extensions)):
        for path in found:
            abs_path = path.absolute()
            print(f"Found: {abs_path} ...")

            with open(abs_path, mode="r+", encoding="utf-8") as f:
                lines = f.readlines()
                contains_shebang = (lines[0][0:2] == "#!")
                shebang_line = (lines[0] + "\n\n" if contains_shebang else "")

                # Skip if already contains license
                contains_license = False
                for line in lines:
                    if "Licensed under the terms of MIT license" in line:
                        contains_license = True
                        break
                if contains_license:
                    print("... already contains license information")
                    continue

                # Add license
                new_lines = generate_license_header(extension2style(extension))
                f.seek(0)
                f.writelines(
                    [shebang_line, new_lines] +
                    (lines[1:] if contains_shebang else ["\n"] + lines)
                )
                print("... added license information")


if __name__ == "__main__":
    main()
