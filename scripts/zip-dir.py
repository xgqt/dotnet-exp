#!/usr/bin/env python3


# (c) 2022 Maciej Barć <xgqt@riseup.net>
# Licensed under the terms of MIT license
# SPDX-License-Identifier: MIT

# Example: ./scripts/zip-dir.py -o $(pwd)/my_zip ./src/exp/make-web-gallery


import argparse

from os import chdir
from subprocess import check_output
from zipfile import ZipFile


def main() -> None:
    """Main."""

    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output", type=str,
                        default="zip", help="output path")
    parser.add_argument("path", type=str)
    args = parser.parse_args()

    chdir(args.path)

    git_files = list(map(
        (lambda p: p.decode("utf-8")),
        check_output(["git", "ls-files", "."]).splitlines()))

    with ZipFile(args.output + ".zip", "w") as zip_file:
        for git_file in git_files:
            print(f"Adding {git_file} to {zip_file.filename} ...")
            zip_file.write(git_file)


if __name__ == "__main__":
    main()
