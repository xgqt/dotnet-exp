#!/bin/sh


# (c) 2022 Maciej Barć <xgqt@riseup.net>
# Licensed under the terms of MIT license
# SPDX-License-Identifier: MIT


trap 'exit 128' INT

where="${1:-$(pwd)}"

# Remove execution permissions from everything with a extension.
find "${where}" -type f  -name "*.*" -exec chmod -x "{}" +

# Add execution permissions to files in scripts sub-directory.
[ -d "${where}/scripts" ] &&
    find "${where}/scripts" -type f -exec chmod +x "{}" +
