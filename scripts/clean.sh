#!/bin/sh


# (c) 2022 Maciej Barć <xgqt@riseup.net>
# Licensed under the terms of MIT license
# SPDX-License-Identifier: MIT


trap 'exit 128' INT

where="${1:-$(pwd)}"

find "${where}" -type d \( -name "bin" -o -name "obj" \) -exec rm -r "{}" +
