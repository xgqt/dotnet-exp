#!/usr/bin/env -S awk -f


# (c) 2022 Maciej Barć <xgqt@riseup.net>
# Licensed under the terms of MIT license
# SPDX-License-Identifier: MIT

# Strip carriage return (CR) symbols from a given file,
# this is similar to running "dos2unix".


{
    "$1"
    gsub(/\r$/, "")  # CR
    gsub(/^\xef\xbb\xbf/, "")  # BOM
    ;
    print > FILENAME
}
