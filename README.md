# .Net-Exp


## About

.NET experiments and exercises for programming classes/assignments.

**WARNING**: Many of programs here run on MS Windows only!


## License

```txt
(c) 2022 Maciej Barć <xgqt@riseup.net>
Licensed under the terms of MIT license
SPDX-License-Identifier: MIT
```
